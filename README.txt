
===========================================
Search API Solr Partial Word Matching
===========================================
Search API Solr Partial Word Matching aims to improve the full text search by adding
the partial word matching functionality.

Please visit:

  http://drupal.org/project/search_api_solr_partial_word_matching

For information, updates, configuration help, and support.

============
Installation
============

1. If you work with composer to install modules in your drupal 7 project:
   composer require drupal/search_api_solr_partial_word_matching

   If you work with drush to install modules in your drupal 7 project:

   - To download the module: drush dl search_api_solr_partial_word_matching
   - To enable the module : drush en search_api_solr_partial_word_matching

2. Next step before using the module !
   Before using the module and the partial word matching functionality,
   you should configure your Apache Solr with schema.xml situated in solr-conf/7.x (the new schema.xml include
   the new functionality - for now, only the 7.x solr-conf is already available).

3.  Go to Configuration > Search API > select tab fields in index related to SOLR SERVICE
    and change Fulltext to Fulltext (partial word matching) type.

======
Author
======

Developed & maintained by Mohamed Anis Taktak.

Email: mohamed [at] websolutions.hr
Drupal: https://www.drupal.org/u/matio89

